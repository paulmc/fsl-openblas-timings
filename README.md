# Performance of FSL tools with OpenBLAS parallelisation

This discussion pertains to FSL 6.0.6, where we are using a version of OpenBLAS which has been compiled to use OpenMP for parallelisation. Only a small selection of FSL tools have been benchmarked (`melodic`, `topup`, `fnirt`), so the results presented here may or may not be applicable to other FSL tools.


In general, enabling BLAS parallelisation only gives minor performance benefits and so, when working at small scale, it is probably not worth the hassle of parallelising. 


In FSL 6.0.6, BLAS parallelisation is explicitly disabled programmatically in all tools, via a call to `openblas_set_num_threads(1)` (see `fslStartup.cc` and `threading.h/cpp`, both in the fsl/utils> project). 


This is because the default settings used by `libgomp` (the GNU OpenMP implementation) result in _poorer_ performance, and an excess of wasted CPU cycles.  The [`GOMP_SPINCOUNT`](https://gcc.gnu.org/onlinedocs/libgomp/GOMP_005fSPINCOUNT.html#GOMP_005fSPINCOUNT) setting controls how long worker threads will busy-spin whilst waiting for a new job to be scheduled; the default behaviour for this setting appears to be far too high for the types of calls that are typical in the FSL tools tested. 


This can be avoided by explicitly setting `GOMP_SPINCOUNT=0` (or equivalently setting `OMP_WAIT_POLICY=PASSIVE`). However, because this is the _default_ behaviour, and because the performance benefits gained by paralellisation are so modest, we have decided to explicitly disable OpenBLAS paralellisation by default for the time being.


However, the initial call to `openblas_set_num_threads(1)` can be skipped by setting an environment variable `FSL_SKIP_GLOBAL=1`. This will have the effect that the standard OpenBLAS environment variables (e.g. `BLAS_NUM_THREADS`) will be honoured.


Some benchmark timings are listed below.


Running `fnirt` on Linux (Dell XPS 9380, four cores):

```
fnirt --in=../STRUCT --ref=$FSLDIR/data/standard/MNI152_T1_2mm --config=$FSLDIR/etc/flirtsch/T1_2_MNI152_2mm.cnf --fout=struct2std_warp

| environment variables set   | total time | CPU% |
|-----------------------------|------------|------|
| OMP_NUM_THREADS=1           | 4:36.56    | 99%  |
|-----------------------------|------------|------|
| OMP_NUM_THREADS=4           | 4:36.55    | 161% |
|-----------------------------|------------|------|
| OPENBLAS_THREAD_TIMEOUT=1   |            |      |
| OMP_NUM_THREADS=4           | 4:48.24    | 161% |
|-----------------------------|------------|------|
| OMP_WAIT_POLICY=PASSIVE     |            |      |
| OPENBLAS_THREAD_TIMEOUT=1   |            |      |
| OMP_NUM_THREADS=4           | 4:34:22    | 99%  |
|-----------------------------|------------|------|
| OMP_WAIT_POLICY=PASSIVE     |            |      |
| OMP_NUM_THREADS=4           | 4:17.97    | 100% |
```

On macOS (Macbook Pro late 2013, 8 cores):

``` 
| environment variables set   | total time | CPU time |
|-----------------------------|------------|----------|
| OMP_NUM_THREADS=1           | 273.48     | 266.30   |
|-----------------------------|------------|----------|
| OMP_NUM_THREADS=8           | 285.03     | 775.38   |
|-----------------------------|------------|----------|
| OPENBLAS_THREAD_TIMEOUT=1   |            |          |
| OMP_NUM_THREADS=8           | 292.08     | 803.15   |
|-----------------------------|------------|----------|
| OMP_WAIT_POLICY=PASSIVE     |            |          |
| OPENBLAS_THREAD_TIMEOUT=1   |            |          |
| OMP_NUM_THREADS=8           | 255.71     | 248.57   |
|-----------------------------|------------|----------|
| OMP_WAIT_POLICY=PASSIVE     |            |          |
| OMP_NUM_THREADS=8           | 257.32     | 249.94   |
```

Running `melodic` on Linux:

```
melodic -i wordgen120vols -o output

| environment variables set | total time | CPU% |
|---------------------------|------------|------|
| OMP_NUM_THREADS=1         | 3:04.15    | 99%  |
|---------------------------|------------|------|
| OMP_NUM_THREADS=4         | 2:57.42    | 134% |
|---------------------------|------------|------|
| OPENBLAS_THREAD_TIMEOUT=1 |            |      |
| OMP_NUM_THREADS=4         | 3:35.28    | 133% |
|---------------------------|------------|------|
| OMP_WAIT_POLICY=PASSIVE   |            |      |
| OPENBLAS_THREAD_TIMEOUT=1 |            |      |
| OMP_NUM_THREADS=4         | 3:05.00    | 105% |
|---------------------------|------------|------|
| OMP_WAIT_POLICY=PASSIVE   |            |      |
| OMP_NUM_THREADS=4         | 3:00.20    | 103% |
```


On macOS:


```
| environment variables set | total time | CPU time |
|---------------------------|------------|----------|
| OMP_NUM_THREADS=1         | 202.11     | 201.10   |
|---------------------------|------------|----------|
| OMP_NUM_THREADS=8         | 209.36     | 454.03   |
|---------------------------|------------|----------|
| OPENBLAS_THREAD_TIMEOUT=1 |            |          |
| OMP_NUM_THREADS=8         | 201.73     | 412.26   |
|---------------------------|------------|----------|      
| OMP_WAIT_POLICY=PASSIVE   |            |          |
| OPENBLAS_THREAD_TIMEOUT=1 |            |          |
| OMP_NUM_THREADS=8         | 193.24     | 211.77   |
|---------------------------|------------|----------|
| OMP_WAIT_POLICY=PASSIVE   |            |          |
| OMP_NUM_THREADS=8         | 163.34     | 173.03   |
```

Running `topup` on Linux (jalapeno19, 16 cores):

```
| environment variables set   | total time | CPU% |
|-----------------------------|------------|------|
| OMP_NUM_THREADS=1           | 4:02.05    | 99%  |
|-----------------------------|------------|------|
| OMP_NUM_THREADS=16          | 4:50.74    | 297% |
|-----------------------------|------------|------|
| OMP_WAIT_POLICY=PASSIVE     |            |      |
| OMP_NUM_THREADS=16          | 4:09.58    | 100% |
```

On macOS:

```
| environment variables set   | total time | CPU time |
|-----------------------------|------------|----------|
| OMP_NUM_THREADS=1           | 187.92     | 186.47   |
|-----------------------------|------------|----------|
| OMP_NUM_THREADS=8           | 209.54     | 475.48   |
|-----------------------------|------------|----------|
| OMP_WAIT_POLICY=PASSIVE     |            |          |
| OMP_NUM_THREADS=8           | 186.54     | 185.14   |
```
